---
layout: activity
title: Groupe 07
autotoc: true
---

### La situation actuelle en Suisse (contexte)

 Expliquer la situation actuelle :
 Conséquence, cause de la disparision des abeilles en Suisse (durant les 10 dernières années)
 Plusieurs facteurs exercent une pression grandissante sur l’abeille : détérioration des habitats, alimentation non adéquate, intoxication aux pesticides, bouleversements causés par les changements climatiques, présence accrue de divers prédateurs, etc.

#### Analyse de sous-systèmes en Suisse

D'ou proviennent les données (Statista, Pronatura, Office de la statistique)
Les effets nocifs des néonicotinoïdes sur les abeilles

#### La situation internationale

 Recherche base de données internationales 

### L' Application de la culture computationnelle

 C'est comment décomposer le système complexe actuel en le schematisant d'une manière compréhensible / simple

#### Notre défis (description générique)

 Pourquoi les abilles sont importantes, pour l environnement, pou r la sociéteé
 Bio cahier des charges 
 donner les causes
problème des agriculeuts

#### Abstraction (ce qu'on cherche à résoudre)

donner la cause princiael
ce qu on veut changer, 
Eclaircir certaines zone d'ombre qui concerne la disparition des abeilles ainsi que ses impacts.

#### Décomposition du problème

 Comprendre la situation actuelle par des fait empiriques
 extraire les éléments abstrait 
 comprendre les différents composant
 proposer un schéma de la pensée computatonnelle 

##### Les utilisateurs

????

##### Procédures principales

 ???

##### Organisations concernés

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Systèmes disponibles

 L???

##### Les données concernées (type)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### L' Application de la pensée computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les patterns à comprendre et réutiliser

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les scenarii

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### "L'algorithme" qui en découle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### Sources

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.
