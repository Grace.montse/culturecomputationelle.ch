---
layout: activity
title: Huile pour Groupe 05
autotoc: true
---

### La situation actuelle en Suisse (contexte)

 Test 02.11.2021.

#### Analyse de sous-systèmes en Suisse

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### La situation internationale

Au niveau mondial, l’huile de palme est la plus produite et échangée avec plus de 52 millions de tonnes consommées en 2021 selon le département de l’Agriculture américain. L’immense majorité - 80% - part vers l’industrie agroalimentaire tandis que le reste va dans les produits cosmétiques ou agro carburants. Cette dernière est principalement produite en Indonésie et Malaisie (+90% du total) et les producteurs locaux agissent très souvent pour le compte de grandes multinationales comme Cargill, Unilever, Nestlé, Kraft Foods ou Procter & Gamble.
Ces mêmes groupes subissent toutefois régulièrement des critiques de la part d’ONG qui les accusent d’utiliser de l’huile de palme produite de manière non durable et, souvent, par du travail forcé d’enfants.

### L' Application de la culture computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Notre défis (description générique)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Abstraction (ce qu'on cherche à résoudre)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Décomposition du problème

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Les utilisateurs

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Procédures principales

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Organisations concernés

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Systèmes disponibles

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Les données concernées (type)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### L' Application de la pensée computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les patterns à comprendre et réutiliser

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les scenarii

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### "L'algorithme" qui en découle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### Sources

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.
